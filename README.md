# Fast almost optimal matching

The codes accompany the paper below and the extended technical report: 

<pre>

@InProceedings{panagiotas_et_al:LIPIcs:2020:12942,
  author ={Ioannis Panagiotas and Bora U{\c{c}}ar},
  title ={{Engineering fast almost optimal algorithms for bipartite graph matching}},
  booktitle ={28th Annual European Symposium on Algorithms (ESA 2020)},
  pages ={76:1--76:23},
  series ={Leibniz International Proceedings in Informatics (LIPIcs)},
  ISBN ={978-3-95977-162-7},
  ISSN ={1868-8969},
  year ={2020},
  volume ={173},
  editor ={Fabrizio Grandoni and Grzegorz Herman and Peter Sanders},
  publisher ={Schloss Dagstuhl--Leibniz-Zentrum f{\"u}r Informatik},
  address ={Dagstuhl, Germany},
  URL ={https://drops.dagstuhl.de/opus/volltexte/2020/12942},
  URN ={urn:nbn:de:0030-drops-129424},
  doi ={10.4230/LIPIcs.ESA.2020.76},
  annote ={Keywords: bipartite graphs, matching, randomized algorithm}
}

@techreport{panagiotas:hal-02463717,
  TITLE = {{Engineering fast almost optimal algorithms for bipartite graph matching: Extended version}},
  AUTHOR = {Panagiotas, Ioannis and U{\c c}ar, Bora},
  URL = {https://hal.inria.fr/hal-02463717v2},
  TYPE = {Research Report},
  NUMBER = {RR-9321},
  INSTITUTION = {{Inria - Research Centre Grenoble -- Rh{\^o}ne-Alpes}},
  YEAR = {2020},
  MONTH = Feb,
  PDF = {https://hal.inria.fr/hal-02463717v2/file/RR-9321.pdf},
  HAL_ID = {hal-02463717},
  HAL_VERSION = {v2},
}

</pre>

We discuss adaptations and efficient implementations of (i) a Monte Carlo algorithm originally designed for random 2-out biparite graphs [1]; and (ii) a Las Vegas algorithm originally 
designed for regular bipartite graphs and bipartite graphs corresponding to doubly stochastic matrices (edges are weighted) [2]. The adapted versions are called **2outmc** and **truncrw**.

* [1] **R. M. Karp,  A. H. G. Rinnooy Kan, and R. V. Vohra**,
*Average case analysis of a heuristic for the assignment problem*,
Mathematics of Operations Research, 19 (3), pp. 513--522, 1994.

* [2] **A. Goel, M. Kapralov, and S. Khanna**, 
*Perfect matchings in $O(n\log n)$ time in regular bipartite graphs*,
SIAM Journal on Computing, 42 (3), pp. 1392--1404, 2013.

## License
See the file LICENSE. The software is under CeCILL-B license, which is a BSD-like license.

## Contact
The e-mail addresses of the authors are:
ioannais.panagiotas@ens-lyon.fr and bora.ucar@ens-lyon.fr.

## Build
The codes are written in C/C++ and are easily callable. 
We provide a mex interface for ease of use in Matlab.

## Versions
This version v0.1 (February 2020).
