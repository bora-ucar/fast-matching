
n = 10000;
degs = [2, 3, 4, 5];

numTrials = 5;
for dd = 1:length(degs)
   d = degs(dd);
   mresult = zeros(numTrials, 1);
   spranks = zeros(numTrials, 1);
   for tt = 1:numTrials
        A = spones(sprand(n, n, d/n));
        [r, c, As, numIters, err] = buScaleSK( A, 10, 0.00001);
         match = truncRWwrapper(As);
         mresult(tt) = length(find(match>0));         
         spranks(tt) = sprank(A);
   end
   [~, minLoc] = min(mresult);
   if(mresult(minLoc)> spranks(minLoc))
       keyboard
   end
   fprintf('%d %d %.4f\n', d, spranks(minLoc), mresult(minLoc)/spranks(minLoc));
end

fprintf('\t\tRectangulars\n');

m = 12000;
numTrials = 5;
for dd = 1:length(degs)
   d = degs(dd);
   mresult = zeros(numTrials, 1);
   spranks = zeros(numTrials, 1);
   for tt = 1:numTrials
        A = spones(sprand(n, m, d/n));
        [r, c, As, numIters, err] = buScaleSK( A, 10, 0.00001);
         match = truncRWwrapper(As);
         mresult(tt) = length(find(match>0));         
         spranks(tt) = sprank(A);
   end
   [~, minLoc] = min(mresult);
   if(mresult(minLoc)> spranks(minLoc))
       keyboard
   end
   fprintf('%d %d %.4f\n', d, spranks(minLoc), mresult(minLoc)/spranks(minLoc));
end