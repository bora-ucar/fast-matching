#include <stdio.h> 
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "utils.h"


#define randv(maxv) (maxv) * (rand()/(double) RAND_MAX)
/*returns a random double between 0 and maxv*/

void createVisitOrder(int *visitOrder, int n)
{
	int i, tmp;
	for(i = 0; i < n; i++) 
		visitOrder[i] = i;

	for(i = n-1; i >= 0; i--) 
	{
		int z = rand() % (i+1);
		tmp = visitOrder[i]; 
		visitOrder[i] = visitOrder[z]; 
		visitOrder[z] = tmp;
	}
}

static inline int sampleLinearTime(int *rids_col, double *vals_col, int sz, int col, int *rowmatch)
{
	int i, r, offset = -1;
	double rsum = 0.0; 
	double rval;
	for (i = 0; i < sz; i++)	/*calculate row sum*/
	{
		if (rowmatch[rids_col[i]] != col)
			rsum += vals_col[i];
	}

	rval = randv(rsum);         /*a random val*/
	rsum = 0.0;
	for (i = 0; i < sz; i++)	
	{
		if(rowmatch[rids_col[i]] != col)
		{
			rsum += vals_col[i];
			if(rsum >= rval)
			{
				offset = i;
				break;
			} 
		}			
	}
	
	return offset;
}


void goelKapralovKhanna(int *cptrs, int *rids, double *vals, int *match, int *rowmatch, int n, int m)
{
	/*This function should not be called without precaution:
	 *     + it does not check the input
	 *     + uses linear time algorithm to sample (for correctness)     	 
	 *     + does not truncate the walks.
	 */    
	/*we are going to sample-out-edge from cols.*/

	int i, j, v, offset, pathLast, col, pcol, row, rtmp;
	int *path, *rowFromWhere, *visitOrder;
	int walkLength; /*For statistics; can be removed for run time*/
	double totalLength = 0.0;
	rowFromWhere = (int *) malloc(sizeof(int) * m);/*if a row is already seen in a randomWalk, its position is kept here*/
	path  = (int *) malloc(sizeof(int) * n);       /*the randomWalk, where the cycles are discarded, so a path*/
	visitOrder = (int *) malloc(sizeof(int) * n);

	createVisitOrder(visitOrder, n);
	for (i = 0; i < m; i++)	
		rowmatch[i] = rowFromWhere[i] = -1;

	for (j = 0; j < n; j++)
		match[j] = -1;

	for (v = 0; v < n; v ++)/*we will do n augmentations from the empty matching*/
	{
		j = visitOrder[v];

		path[0] = j; 
		pathLast = 0;
		if(match[j] != -1)
			myExit("already matched\n");

		walkLength = 0;/*For statistics; can be removed for run time*/
		while (pathLast >= 0)
		{
			col = path[pathLast];
			/*pick a random unmatched row from this col*/
			offset = sampleLinearTime(&(rids[cptrs[col]]), &(vals[cptrs[col]]), cptrs[col+1] - cptrs[col], col, rowmatch);			
			row = rids[cptrs[col] + offset];
			walkLength ++;/*For statistics; can be removed for run time*/
			totalLength += 1.0;
			if(rowmatch[row] != - 1)/*not an augmenting path*/
			{
				/*grow the path*/
				if( rowFromWhere[row] == -1 )/* this row is new for randomWalk at j*/
				{
					rowFromWhere[row] = pathLast+1;					
					path[++pathLast] = rowmatch[row];
					walkLength++;
				}
				else/*we have already seen this row in this randomWalk, discard the cycle*/
				{
					/*from pathLast down to romFromWhere[row], we should discard the vertices; they form the cycle*/
					while(pathLast > rowFromWhere[row])
						rowFromWhere[match[path[pathLast--]]] = -1;													
				}
			}	
			else/*found an augmenting path*/
			{
				for ( ; pathLast >= 0; pathLast--)
				{
					pcol = path[pathLast];
					rtmp = match[pcol];
					match[pcol] = row; 
					rowFromWhere[row] = -1;
					rowmatch[row] = pcol;
					row = rtmp;					
				}
				break;
			}
		}	
	}
	myPrintf("total walk length %.0f and limit %.0f\n", totalLength, n  * log(1.0*n));/*For statistics; can be removed for run time*/

	free(visitOrder);
	free(path);
	free(rowFromWhere);
}

/****************************************************************************
*Truncated walk version.
*****************************************************************************/
#define matchingEdgeVal(vals, offset) offset == 0 ? vals[offset] : 	vals[offset] - vals[offset - 1]

static inline void prefixSum(double *vals_col, int sz)
{
	int i;
	for (i = 1; i < sz; i++)
		vals_col[i] += vals_col[i-1];
}

static inline int binSearch(double *vals_col, int sz, double rval)
{
	int l = 0;
	int r = sz-1;

	while (l <= r)
	{
		int m = l + (r-l)/2;
		if(l == r-1)
		{
			if (vals_col [l] < rval)
				return r;
			else 
				return l;
		}
		else 
		{
			if (l == r)
				return l;
		}
		if(vals_col [m] < rval)
			l = m +1 ;
		else				
			r = m ;
	}
	return -1;/*in case the list was empty or we could not find*/
}

static inline int sampleLognTime(int *rids_col, double *vals_col, int sz, int col, int itsmate, int offset, int *rowmatch, int *lookAhead, int base, int LAend )
{
	double medgeVal = 0.0, rsum, rval;
	int newoffset = -1, rid;

	while(newoffset == -1 && lookAhead[col] < LAend)/*look ahead mechanism*/
	{
		rid = rids_col[lookAhead[col] - base];	
		if(rowmatch[rid] == -1)
			newoffset = lookAhead[col] - base;		
		lookAhead[col]++;
	}

	if(newoffset == -1)/*look a head did not find anything*/
	{
		if (itsmate != -1)	
			medgeVal = matchingEdgeVal(vals_col, offset); 

		rsum = vals_col[sz - 1] - medgeVal;
		rval = randv(rsum);         /*a random val*/

		if (itsmate == -1)/*		binary search in the whole list*/ 
			newoffset = binSearch(vals_col, sz, rval);	
		else
		{
			if(vals_col [offset] - medgeVal >= rval || offset == sz - 1)/*search 0 -- offset -1*/
			newoffset = binSearch(vals_col, offset , rval);
			else/*search vals_col[offset +1 ...sz], the value (rval + medgeVal)*/	
			newoffset = binSearch(&(vals_col[offset + 1]), sz-offset -1, rval + medgeVal) + offset + 1;	
		}
	}	

	return newoffset;

}
void goelKapralovKhannaTruncated(int *cptrs, int *rids, double *vals, int *match, int *rowmatch, int n, int m)
{
	/*we have m=n, but keeping it clean.*/
	/*we are going to sample-out-edge from cols.*/
	int i, j, v, offset, pathLast, col, pcol, row, rtmp;
	int *path, *rowFromWhere, *visitOrder, *medgeOffsets, *newMedgeOffsets, *lookAhead;
	int walkLength, numTrials, cardMatch; 
	double totalLength;

	rowFromWhere = (int *) malloc(sizeof(int) * m);/*if a row is already seen in a randomWalk, its position is kept here*/
	path  = (int *) malloc(sizeof(int) * n);       /*the randomWalk, where the cycles are discarded, so a path*/
	visitOrder = (int *) malloc(sizeof(int) * n);
	medgeOffsets = (int *) malloc(sizeof(int) * n);
	newMedgeOffsets = (int *) malloc(sizeof(int) * n);
	lookAhead = (int *) malloc(sizeof(int) * n);

	srand(time(NULL));

	createVisitOrder(visitOrder, n);
	for (i = 0; i < m; i++)	
		rowmatch[i] = rowFromWhere[i] = -1;

	for (j = 0; j < n; j++)
	{
		match[j] = 	medgeOffsets[j]= -1;
		prefixSum(&(vals[cptrs[j]]), cptrs[j+1] - cptrs[j]);
		lookAhead[j] = cptrs[j];
	}

	cardMatch = 0;
	totalLength = 0.0;
	for (v = 0; v < n; v ++)/*we will do n augmentations from the empty matching*/
	{
		j = visitOrder[v];

		if(match[j] != -1)
			myExit("already matched\n");
		if(cptrs[j+1] - cptrs[j] == 0)/*skip empty columns*/
			continue;
		if(cptrs[j+1] - cptrs[j] == 1 & match[j] != -1)
			continue;
		numTrials = 0;
		while(match[j] == -1 && numTrials < 1)/*success with probability 1-1/2^10, about 0.999*/
		{
			walkLength = 0;
			path[0] = j; 
			pathLast = 0;
			numTrials ++;
			while (pathLast >= 0 && walkLength <= (int) 2 *(4 + ceil ( (2.0 * n)/(n-v))))
			{
				col = path[pathLast];
				/*pick a random unmatched row from this col*/
				offset = sampleLognTime(&(rids[cptrs[col]]), &(vals[cptrs[col]]), cptrs[col+1] - cptrs[col], col,  match[col], medgeOffsets[col], rowmatch, lookAhead, cptrs[col], cptrs[col+1]);
	
				if(offset == -1) /*Guarding against vertices with degreees 0 or 1s*/							
					break;	
							
				row = rids[cptrs[col] + offset];
				newMedgeOffsets[col] = offset;

				walkLength ++;
				totalLength += 1.0;
				if(rowmatch[row] != - 1)/*not an augmenting path*/
				{	/*grow the path*/
					if(rowFromWhere[row] == -1 )/* this row is new for randomWalk at j*/
					{
						rowFromWhere[row] = pathLast+1;
						path[++pathLast] = rowmatch[row];
						walkLength++; 
					}
					else/*we have already seen this row in this randomWalk, discard the cycle*/
					{   /*from pathLast down to romFromWhere[row], we should discard the vertices; they form the cycle*/
						while(pathLast > rowFromWhere[row])
							rowFromWhere[match[path[pathLast--]]] = -1;
					}
				}	
				else/*found an augmenting path*/
				{
					cardMatch++;
					for ( ; pathLast >= 0; pathLast--)
					{
						pcol = path[pathLast];
						rtmp = match[pcol];
						medgeOffsets[pcol] = newMedgeOffsets[pcol];
						match[pcol] = row; 
						rowFromWhere[row] = -1;
						rowmatch[row] = pcol;
						row = rtmp;					
					}
					break;
				}
			}/*..."while" randomly walking*/
			for ( ; pathLast >= 0; pathLast--)/*clean up rowFromWhere*/
			{
				pcol = path[pathLast];
				row = match[pcol];	
				if(row != -1)
					rowFromWhere[row] = -1;
			}
		}
	}
	myPrintf("total walk length %.0f, match %d\n", totalLength, cardMatch);
	free(lookAhead);
	free(newMedgeOffsets);
	free(medgeOffsets);
	free(visitOrder);
	free(path);
	free(rowFromWhere);
}

