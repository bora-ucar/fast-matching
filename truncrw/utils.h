#include <stdio.h> 
#include <stdlib.h>
#include <sys/time.h>/*for gettimeofday*/
#include <time.h>


#ifdef MATLAB_MEX
	#include "mex.h"
	#define myPrintf mexPrintf
	#define myExit mexErrMsgTxt
#else
	#define myPrintf printf
	#define myExit(s) {\
printf("An error occured: %s\n", s);\
fflush(stdout);\
exit(1);\
}
#endif

double u_seconds(void);

void goelKapralovKhanna(int *cptrs, int *rids, double *vals, int *match, int *rowmatch, int n, int m);
void goelKapralovKhannaTruncated(int *cptrs, int *rids, double *vals, int *match, int *rowmatch, int n, int m);

