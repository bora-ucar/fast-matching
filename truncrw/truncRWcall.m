function [match, mtime] = truncRWcall(A)
%[match, mtime] = truncRWcall(A)
%
%Takes a matrix, scales it (Sinkhorn--Knopp 5 iters) and then 
%      calls truncRWwrapper.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%18 Jan 2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%See also truncRWwrapper.c, matchingRandomWalk.c
[m,n] = size(A);
if (m < n)
    A = A';
    [m,n] = size(A);
end
A = spones(A);

csum = full(sum(A, 1));
rsum = full(sum(A, 2));
mdeg = min(min(csum), min(rsum));
if(mdeg < 2)
   fprintf('minimum degree is %d, we expected at least 2\n', mdeg); 
end

%% Perform scaling
t0 = tic;
[r, c, As, numIters, err] = buScaleSK( A, 5, 0.00001);
tscale= toc(t0);

csum = full(sum(As, 1));
rsum = full(sum(As, 2));
err = max([abs(n/m-max(rsum)), abs(1-max(csum)),abs(n/m-min(rsum)), abs(1-min(csum))]);
fprintf("scalign error %4f, with %.2f seconds\n", err, tscale);

%% Call truncRW
[match, mtimeTmp] = truncRWwrapper(As);

%% Prepare output
if(nargout == 2)
   mtime = mtimeTmp; 
end
return
%% Sanity check. One should skip these, if slows down experimentation.
freev = sum(match < 1);
if(freev)
    fprintf("there are %d free rows\n", freev);
end

% check matching
myrows = match > 0;
mycols = match(myrows);
dd = spdiags(A(myrows, mycols), 0);
if(any(dd ~= 1))
    error("truncRWcall: faulty matching\n");
end

end%function