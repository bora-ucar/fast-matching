The main entry point is `[match, mtime] = truncRWcall(A)`,
with a sparse matrix `A` and with outputs 

 + `match` : `match(i) = j`, if row `i`is matched to column `j`, when `j>0`; otherwise row `i` is not matched.  
 + `mtime` : the time spent in finding the matching.

First need to compile the executables by calling `compileTruncRW` in Matlab.


