#include <stdio.h>
#include <stdlib.h>
#include "utils.h"


double u_seconds(void)
{
    struct timeval tp;
    
    gettimeofday(&tp, NULL);
    return (double) tp.tv_sec + (double) tp.tv_usec / 1000000.0;
    
}