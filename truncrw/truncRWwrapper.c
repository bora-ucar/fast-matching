#include "mex.h"
#include "utils.h"

/*This is a wrapper to get the bipartite graph (weighed ) such that it is doubly stochastic from matlab*/

/*inputs*/
#define A_IN          ( prhs[0] )
#define MATCH_OUT     ( plhs[0] )
#define TIME_OUT      ( plhs[1] )

void mexFunction( int nlhs, mxArray *plhs[],
	int nrhs, const mxArray *prhs[])
{

	mwSize _m, _n, _nz;	
	mwIndex *irn_in;
	mwIndex *jcn_in;
	double *aval_in;

	int i, m, n, nz, j; /*will be copied from matlab*/
	int *cptrs;
	int *rids;
	double *vals;
	int *match, *rowmatch;
	double *match_pr;
    double t0, secs, *tmp_ptr;

	if (!(nrhs == 1 && (nlhs == 1 || nlhs == 2)))
		mexErrMsgTxt("use [match, <runtime(.s)>] = truncRWwrapper(A)\n");
	if (!mxIsSparse(A_IN) )
		mexErrMsgTxt ("First parameter must be a sparse matrix");

	_n = mxGetN(A_IN);
	_m = mxGetM(A_IN);
	jcn_in = mxGetJc(A_IN);
	irn_in = mxGetIr(A_IN);
	_nz = jcn_in[_n];

	n = (int)_n; m = (int) _m; nz = (int) _nz;

	rowmatch = (int *) malloc(sizeof(int) * m);
	match = (int*) mxCalloc(sizeof(int), n);
	vals = (double *) mxCalloc(sizeof(double), nz);
	cptrs =  (int *) mxCalloc(sizeof(int), (n+1));
	rids =(int *) mxCalloc(sizeof(int), (nz));

	for(j = 0; j<=n; j++) 
		cptrs[j] = (int) jcn_in[j];
	
	for(j = 0; j<nz; j++) 
		rids[j] = (int) irn_in[j];
	
	aval_in = mxGetPr(A_IN);
	for(j = 0; j<nz; j++) 
		vals[j] = aval_in[j];

    t0 = u_seconds();
	goelKapralovKhannaTruncated(cptrs, rids, vals, match, rowmatch, n, m);
    secs = u_seconds()-t0;

	MATCH_OUT = mxCreateDoubleMatrix(m, 1, mxREAL);
	TIME_OUT = mxCreateDoubleMatrix(1, 1, mxREAL);
    tmp_ptr = mxGetPr(TIME_OUT);
    *tmp_ptr = secs;

	match_pr  = mxGetPr(MATCH_OUT);
	for (i = 0; i < m; i++) 
	{
		if(rowmatch[i] == -1) 
			match_pr[i] = -1;
		else 
			match_pr[i] = rowmatch[i] + 1;		
	}

	mxFree(match);
	mxFree(cptrs);	
	mxFree(rids);
	mxFree(vals);
	return;
}
