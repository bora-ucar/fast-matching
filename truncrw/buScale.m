function As = buScale(A, Dr, Dc)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% As = buScale(A, Dr, Dc)
% Scales A with
%  As = spdiags(ones(size(D))./D, 0, n,n)* A *spdiags(ones(size(D))./D, 0, n, n);
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% references: 
%            A Symmetry preserving algorithm for matrix scaling,
%            P.A. Knight, D. Ruiz, and B. Ucar,
%            SIAM Journal on Matrix Analysis and Applications, 35 (3), 2014, pp. 931--955
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[m,n] = size(A);
As = spdiags(ones(size(Dr))./Dr, 0, m, m) * A * spdiags(ones(size(Dc))./Dc, 0, n, n);

