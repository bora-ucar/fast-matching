#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <cstring>
#include "mex.h"
#include <sys/time.h>
#include "twoutmcheur.h"
#define MARKED  1
#define UNMARKED  0
#define CHECKED 1
#define UNCHECKED 0
//#include "udynkrvh.h"

/*inputs*/
#define A_IN          ( prhs[0] )
#define SR_IN (prhs[1])
#define SC_IN (prhs[2])
#define BOYS_IN (prhs[3])
#define GIRLS_IN (prhs[4])
/*outputs*/
 #define MATCH_OUT   (plhs[0])
 #define TIME_OUT ( plhs[1] )
#define BOYS_OUT (plhs[2])
#define GIRLS_OUT (plhs[3])
/*...should have the following two lines for matlab version <= 7.1*/
/*
 * typedef int mwIndex;
 * typedef int mwSize;
 */

void getmatching(int n,int *boys,int *girls,int *bg_marked,int *gg_checked,int *match){

    int  *rxids=(int*)malloc((n+1)*sizeof(int));
    int  *cxids=(int*)malloc((n+1)*sizeof(int));
    int  *rdegree=(int*)malloc(n*sizeof(int));
    int  *cdegree=(int*)malloc(n*sizeof(int));    
    int *rmatch=(int*)malloc(n*sizeof(int));
    int num_edges=0;  
    for (int i=0;i<n;++i) { 
      rdegree[i]=0; 
      cdegree[i]=0;
      rmatch[i]=-1;
      match[i]=-1;
    }
    for (int i=0;i<n;++i){
      if (bg_marked[i]==MARKED){
        int b1=girls[2*i];
        int b2=girls[2*i+1];
        
        if (gg_checked[b1]==CHECKED){
          rdegree[i]++;
          cdegree[b1]++;
          num_edges++;
        }else{
        }
        if (gg_checked[b2]==CHECKED){
          rdegree[i]++;
          cdegree[b2]++;
          num_edges++;
        }else{
        }
      } 
      if (gg_checked[i]==UNCHECKED){
        int g1=boys[2*i];
        int g2=boys[2*i+1];
        if (bg_marked[g1]==UNMARKED){
          cdegree[i]++;
          rdegree[g1]++;
          num_edges++;
        }
        if (bg_marked[g2]==UNMARKED){
          cdegree[i]++;
          rdegree[g2]++;
          num_edges++;
        }
      }
    } 
    
    int *rids=(int*)malloc(num_edges*sizeof(int));
    int *cids=(int*)malloc(num_edges*sizeof(int));
    rxids[0]=0;
    cxids[0]=0;

    for (int i=1;i<=n;++i){
      rxids[i]=rxids[i-1]+rdegree[i-1];
      cxids[i]=cxids[i-1]+cdegree[i-1];

      rdegree[i-1]=rxids[i]-1;
      cdegree[i-1]=cxids[i]-1;
    }
    for (int i=0;i<n;++i){
      if (bg_marked[i]==MARKED){
        int b1=girls[2*i];
        int b2=girls[2*i+1];
        if (gg_checked[b1]==CHECKED){
          rids[rdegree[i]--]=b1;
          cids[cdegree[b1]--]=i;
        }
        if (gg_checked[b2]==CHECKED){
          rids[rdegree[i]--]=b2;
          cids[cdegree[b2]--]=i;
        }
      } 
      if (gg_checked[i]==UNCHECKED){
        int g1=boys[2*i];
        int g2=boys[2*i+1];
        if (bg_marked[g1]==UNMARKED){
          cids[cdegree[i]--]=g1;
          rids[rdegree[g1]--]=i;
        }
        if (bg_marked[g2]==UNMARKED){
          cids[cdegree[i]--]=g2;
          rids[rdegree[g2]--]=i;
        }
      }
    }   
    karpSipser(n, cxids, cids,rxids,rids,match,rmatch);
    free(cxids);
    free(cids); 
    free(rxids);
    free(rids);
    free(rmatch);
}
double u_seconds(void)
{
    struct timeval tp;
    
    gettimeofday(&tp, NULL);
    return (double) tp.tv_sec + (double) tp.tv_usec / 1000000.0;
    
}



void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  int i, j, nz;

  mwSize _m, _n, _nz;
  mwIndex *irn_in;
  mwIndex *jcn_in;

  int m, n,row;

  int* col_ptrs;
  int* col_ids;
  int *row_ptrs;
  int *row_ids;
  int ep,sp;

  if (!mxIsSparse(A_IN)) {
    mexErrMsgTxt("First parameter must be a sparse matrix");
  }

  int *boys, *girls;
  double *sr, *sc;
  double *inSC;
  double *inSR;
  double *inBC;
  double *inGC;
  double  *output_mch, *output_time, *output_bc, *output_gc;
  _n = mxGetN(A_IN);
  _m = mxGetM(A_IN);
  jcn_in = mxGetJc(A_IN);
  irn_in = mxGetIr(A_IN);
  inSC= mxGetPr(SC_IN);
  inSR=mxGetPr(SR_IN);
  inBC=mxGetPr(BOYS_IN);
  inGC=mxGetPr(GIRLS_IN);
  _nz = jcn_in[_n];

  n = (int)_n; m = (int) _m; nz = (int) _nz;
  
  if(n != _n || m != _m || nz != _nz) {
    mexErrMsgTxt("problems with conversion...will abort.");
  }

  col_ptrs =  (int *) mxCalloc(sizeof(int), (n+1));
  col_ids =(int *) mxCalloc(sizeof(int), (nz));
  for(j = 0; j<=n; j++) {
    col_ptrs[j] = (int) jcn_in[j];
  }
  for(j = 0; j<nz; j++) {
    col_ids[j] = (int) irn_in[j];
  }
  row_ptrs = (int*) malloc((m+1) * sizeof(int));
  memset(row_ptrs, 0, (m+1) * sizeof(int));
   nz = col_ptrs[n];
  for(i = 0; i < nz; i++) {row_ptrs[col_ids[i]+1]++;}
  for(i = 0; i < m; i++) {row_ptrs[i+1] += row_ptrs[i];}
  int* t_row_ptrs = (int*) malloc(m * sizeof(int));
  memcpy(t_row_ptrs, row_ptrs, m * sizeof(int));
  row_ids = (int*) malloc(nz * sizeof(int));
  for(i = 0; i < n; i++) {
    sp = col_ptrs[i];
    ep = col_ptrs[i+1];
    for(;sp < ep; sp++) {
      row = col_ids[sp];
      row_ids[t_row_ptrs[row]++] = i;
    }
  }
  free(t_row_ptrs); 
  
  boys=(int*)malloc(2*n*sizeof(int));
  girls=(int*)malloc(2*n*sizeof(int));
  sr=(double*)malloc(n*sizeof(double));
  sc=(double*)malloc(n*sizeof(double));

   for (int i=0;i<n;++i){
    sr[i]=inSR[i];
    sc[i]=inSC[i];
      
   }     
   for (int i=0;i<2*n;++i){
      boys[i]=(int)(inBC[i]-1);
      girls[i]=(int)(inGC[i]-1);
   }
    MATCH_OUT= mxCreateDoubleMatrix(1,(mwSize)n,mxREAL);
    TIME_OUT = mxCreateDoubleMatrix(1,(mwSize)1,mxREAL);
    BOYS_OUT= mxCreateDoubleMatrix(1,(mwSize)2*n,mxREAL);
    GIRLS_OUT= mxCreateDoubleMatrix(1,(mwSize)2*n,mxREAL);
    double t0, secs;
    output_mch=mxGetPr(MATCH_OUT);
    output_time=mxGetPr(TIME_OUT);
    output_bc=mxGetPr(BOYS_OUT);
    output_gc=mxGetPr(GIRLS_OUT);
    int *bg_marked=(int*)malloc(n*sizeof(int));
    int *gg_checked=(int*)malloc(n*sizeof(int));
    int *match=(int*)malloc(n*sizeof(int));

    t0= u_seconds();
    twoutmcheur(n, boys,girls,bg_marked,gg_checked,row_ptrs,row_ids,col_ptrs,col_ids,sr,sc);
    getmatching(n,boys,girls,bg_marked,gg_checked,match);
     secs = u_seconds()-t0;
    for (int i=0;i<n;++i){
      output_mch[i]=(double)(1+match[i]);     
    } 
  *output_time=secs;
  for (int i=0;i<2*n;++i){
    output_bc[i]=(double)(boys[i]+1);
    output_gc[i]=(double)(girls[i]+1);
  }
  mxFree(col_ptrs);
  mxFree(col_ids);
 // mxFree(boys);
//  mxFree(girls);
//  mxFree(bg_marked);
 // mxFree(gg_checked);
  free(sr);
  free(sc);
  free(bg_marked);
  free(gg_checked);

  return;
}


