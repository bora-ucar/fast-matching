function [match, mtime] = twoutMCcall(A)
%[match, mtime] = twoutMCcall(A)
%
%Takes a matrix, scales it (Sinkhorn--Knopp 5 iters) and then 
%      calls twoutMC's mex wrapper.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1 Feb 2020
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%See also twoutmcmex.cpp, twoutmc.cpp
[m,n] = size(A);
if (m ~= n)
    A = A';
    error('expected square matrix');
end
A = spones(A);

csum = full(sum(A, 1));
rsum = full(sum(A, 2));
mdeg = min(min(csum), min(rsum));
if(mdeg < 2)
   error('minimum degree is %d, we expected at least 2\n', mdeg); 
end

%% Perform scaling
[r, c, As, numIters, err] = buScaleSK( A, 5, 0.00001);
csum = full(sum(As, 1));
rsum = full(sum(As, 2));
err = max([abs(n/m-max(rsum)), abs(1-max(csum)),abs(n/m-min(rsum)), abs(1-min(csum))]);
fprintf("scalign error %4f\n", err);
%% Call truncRW
[match, mtimeTmp,~,~] = twoutmcmex(A,r,c);

%% Prepare output
if(nargout == 2)
   mtime = mtimeTmp; 
end
return
freev = sum(match < 1);
if(freev)
    fprintf("there are %d free rows\n", freev);
end


%% will report caerdinality of the matching, can take time.
matchcard = length(find(match > 0));
mxc = sprank(A);
if(matchcard ~= mxc )
    fprintf("matching cardinality %d, size %d\n", matchcard, mxc);
end

%% sanity check matching
myrows = match > 0;
mycols = match(myrows);
dd = spdiags(A(myrows, mycols), 0);
if(any(dd ~= 1))
    error("truncRWcall: faulty matching\n");
end

end%function
