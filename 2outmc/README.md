to compile: type `compiletwoutmc` in MATLAB.

the main entry is `[match, mtime] = twoutMCcall(A);`

while calls a mex function, whose signature is

`[match,t,columns,rows]=twoutmcmex(A,r,c, cols,rws)`

with inputs
 + A : sparse matrix
 + r : scaling vector  (ones(1,size(A,1)) for uniform)
 + c : scaling vector (ones(1,size(A,1)) for uniform)
 + cols:  columns choices can be given as input (in which case columns = cols)
 + rws: rows choices can be given as input (in which case rows =rws)
if `cols[1]==0`,  code will sample for rows and columns.


and with outputs

 + `match` : the matching found by the algorithm  such that if `match[i]!=0`, col. `i` is matched with `match[i]` 
 + `t`     : run time
 + `columns`: the two choices for each column (can be equal to cols if cols was nonzero before) 
 + `rows`  : the two choices per each row (can be equal to rws if rws was nonzero before)
