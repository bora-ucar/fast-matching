#include <vector>
#include "twoutmc.h"
//#include "ks.c"
#include <time.h>
#define CORE 0
#define TREE 1
#define FRINGE 2
#define DEAD 3
#define MARKED  1
#define UNMARKED  0
#define CHECKED 1
#define UNCHECKED 0
using namespace std;

	int  *v1_bfs;
	int  *v2_bfs;
	int  *q1_bfs;
	int  *q2_bfs;
	int r;
	int ts_p;
	int ts_fc;
int min(int a,int b){
	if (a<=b) return a;
	return b;
}
int timer=0;
int bg_treepos=0;

void del_edge(int index,int u,int v,int *type, int *bg_xids,edge *bg_ids,int *bg_degree,int *bg_marked,int *cdegree,int *bg_treestack,vector<vector<int>> &trees,treev *bg_treevert){
	int pos=0;
	int cpos=0;
	int type_=type[index];
	type[index]=DEAD;
	bg_degree[u]--;
	bg_degree[v]--;
	if (type_==CORE){ //EASY-CASE FIRST
		cdegree[u]--;
		cdegree[v]--;
		int ntemp=bg_treepos;
	    cycleRelabel(u,bg_xids, bg_ids, type,cdegree,q1_bfs,trees,&ntemp,bg_treevert,bg_treestack);
	    if (ntemp>bg_treepos){
	    	bg_treepos++;
	    }
	    ntemp=bg_treepos;
	    cycleRelabel(v,bg_xids, bg_ids, type,cdegree,q1_bfs,trees,&ntemp,bg_treevert,bg_treestack);
	     if (ntemp>bg_treepos){
	    	bg_treepos++;
		}
	}else{//next cases are combined!
		r++;
		int pos1=0; int pos2=0;
		int cpos1=0; int cpos2=0;
		int i_pos1=bg_xids[u]; int i_pos2=bg_xids[v];
		v1_bfs[u]=r;
		v2_bfs[v]=r;
		int edges1=bg_degree[u]; int n1=1;
		int edges2=bg_degree[v]; int n2=1;
		q1_bfs[pos1++]=u; q2_bfs[pos2++]=v;
		int marked_unmarked_1=-1;
		int marked_unmarked_2=-1;
		int treevertice=-1;
		while (cpos1<pos1 && cpos2<pos2){ //do  BFS  parallel
			int x1=q1_bfs[cpos1];
			if (cdegree[x1]>0){
				treevertice=v;
				break;
			}
			if (i_pos1==bg_xids[x1+1]){
					cpos1++;
					if (cpos1==pos1) break;
					x1=q1_bfs[cpos1];
					i_pos1=bg_xids[x1];
			}
			if (bg_marked[x1]==MARKED) marked_unmarked_1=1;
			int eid=bg_ids[i_pos1].which;
			int y=bg_ids[i_pos1].to;
			i_pos1++;
			if (v1_bfs[y]!=r && type[eid]!=DEAD){
				v1_bfs[y]=r;
				q1_bfs[pos1++]=y;
				edges1+=bg_degree[y];
				n1++;
			}	
			int x2=q2_bfs[cpos2];
			if (cdegree[x2]>0){
				treevertice=u;
				break;
			}
			if (i_pos2==bg_xids[x2+1]){
					
					cpos2++;
					if (cpos2==pos2) break;
					x2=q2_bfs[cpos2];
					i_pos2=bg_xids[x2];
			}
			if (bg_marked[x2]==MARKED) marked_unmarked_2=1;

			 eid=bg_ids[i_pos2].which;
			 y=bg_ids[i_pos2].to;
			i_pos2++;
				if (v2_bfs[y]!=r && type[eid]!=DEAD){
					v2_bfs[y]=r;
					q2_bfs[pos2++]=y;
					edges2+=bg_degree[y];
					n2++;
				}
			
		}
		if (type_==FRINGE){
			if (treevertice==-1){
				treevertice=u; //which one has the tree?
				if (pos2==cpos2){ //if v has finished has the right amount of edges, v is in tree
				if (n2-1==edges2/2)
					treevertice=v;
				}else{
				if (n1-1!=edges1/2) //similarly, if u has finished and u's not tree its v
					treevertice=v;
				}
			}
			r++;
			cpos1=0;
			pos1=0;
			v1_bfs[treevertice]=r;
			q1_bfs[pos1++]=treevertice;
			int newtreeid=trees.size();
			trees.push_back(vector<int>());
			while (cpos1<pos1){ //relabel all fringe to tree 
					int x1=q1_bfs[cpos1]; 
					bg_treevert[x1].pos=cpos1; cpos1++;
					bg_treevert[x1].treeid=newtreeid;
					bg_treevert[x1].order=2;
					trees[newtreeid].push_back(x1);
					for (int ptr=bg_xids[x1];ptr<bg_xids[x1+1];++ptr){
						int eid=bg_ids[ptr].which;
						int y=bg_ids[ptr].to;
						if (v1_bfs[y]!=r && type[eid]!=DEAD){
							v1_bfs[y]=r;
							q1_bfs[pos1++]=y;
							type[eid]=TREE;
						}
						
					}
			}
			bg_treestack[bg_treepos++]=newtreeid;
	}else{
		int newtreeid=trees.size();
		trees.push_back(vector<int>());
		int oldtree=bg_treevert[u].treeid;
		if (pos1==cpos1){
			int jc1=0;
			for (int j=0;j<pos1;++j){
				int x=q1_bfs[j];
				int xpos=bg_treevert[x].pos;
				if (xpos!=-1){
					int ts=trees[oldtree].size()-1;
					if (xpos!=ts){
						int last=trees[oldtree][ts];
						trees[oldtree].pop_back();
						trees[oldtree][xpos]=last;
						bg_treevert[last].pos=xpos;
					}else{
						trees[oldtree].pop_back();
					}
					bg_treevert[x].pos=jc1++;
					trees[newtreeid].push_back(x);				
				}
				bg_treevert[x].treeid=newtreeid;
			}
			if (marked_unmarked_1==-1){
				bg_treestack[bg_treepos++]=newtreeid;
			}else{
				if (trees[oldtree].size()>0)
					bg_treestack[bg_treepos++]=oldtree;
			}
		}	
		else{
			int jc1=0;
			for (int j=0;j<pos2;++j){
				int x=q2_bfs[j];
				int xpos=bg_treevert[x].pos;
				if (xpos!=-1){
					int ts=trees[oldtree].size()-1;
					if (xpos!=ts){
						int last=trees[oldtree][ts];
						trees[oldtree].pop_back();
						trees[oldtree][xpos]=last;
						bg_treevert[last].pos=xpos;
					}else{
						trees[oldtree].pop_back();	
					}
					bg_treevert[x].pos=jc1++;
					trees[newtreeid].push_back(x);	
				}
				bg_treevert[x].treeid=newtreeid;
			}
			if (marked_unmarked_2==-1){
				bg_treestack[bg_treepos++]=newtreeid;
			}else{
				if (trees[oldtree].size()>0)
					bg_treestack[bg_treepos++]=oldtree;
			}
		} 
	
	}	
			
	}
}
void twoutmc(int n, int *boys,int *girls,int *bg_marked,int *gg_checked,int *rxids,int *rids,int *cxids,int *cids,double *sr, double *sc){
	int *cdegree=(int*)malloc(n*sizeof(int)); //1
	int *bg_xids=(int*)malloc((n+1)*sizeof(int)); //2
	edge *bg_ids= bg_ids= (edge*)malloc((2*n)*sizeof(edge)); //3
	int *bg_degree=(int*)malloc((n)*sizeof(int)); //4
	int *type=(int*)malloc(n*sizeof(int)); //5
	int *bg_treestack=(int*)malloc(n*sizeof(int));//8
	vector<vector<int>> trees;
	treev *bg_treevert=(treev*)malloc(n*sizeof(treev));
	srand(time(NULL));
	int *gg_UF=(int*)malloc(n*sizeof(int)); //9
	int *gg_UF_size=(int*)malloc(n*sizeof(int)); //10
 	int *gg_UF_unchecked=(int*)malloc(n*sizeof(int)); //11
 	r = 0;
 	 q1_bfs=(int*)malloc(n*sizeof(int));
 	q2_bfs=(int*)malloc(n*sizeof(int));
	v1_bfs=(int*)malloc(n*sizeof(int));
 	v2_bfs=(int*)malloc(n*sizeof(int));	
 	for (int i=0;i<n;++i){
 		v1_bfs[i]=-1; v2_bfs[i]=-1;
 	}
 	bg_treepos = 0;
	 if (boys[0]==-1){
	 	select_oneside(n,cxids,cids,sr, boys);
 		select_oneside(n,rxids,rids,sc, girls);
 	}
 	krv_init(n,boys,bg_marked,bg_degree,bg_xids,type,cdegree,bg_ids,gg_UF,gg_UF_size,gg_UF_unchecked,gg_checked,bg_treevert);
 	int nt;
 	find_core( n,bg_xids,bg_ids,bg_marked,type,cdegree,boys,bg_treestack,trees,bg_treevert,&nt,q1_bfs,v1_bfs);
 	bg_treepos=nt;
 	int bg_treecpos=0;
 	while (bg_treecpos<bg_treepos){ //while trees with no marked vertice exist 
		//STEP-1: Find  one such tree & a vertice of tree 
				int treeid=bg_treestack[bg_treecpos++];
		while (trees[treeid].size()>0) {
			int p,x;
				p= rand()% trees[treeid].size();
				x=trees[treeid][p];
				int lastp=trees[treeid].size()-1;
				if (p!=lastp){
					int lastx=trees[treeid][lastp]; 
					bg_treevert[lastx].pos=p;
					trees[treeid][p]=lastx;
				}
				trees[treeid].pop_back();
			bg_treevert[x].pos=-1;
		
				int u=girls[2*x];
				int v=girls[2*x+1];
				int ru=uf_find(u,gg_UF);
				int rv=uf_find(v,gg_UF);
			
				int mu=gg_UF_unchecked[ru]; //there is at most one unmarked in each component
				int mv=gg_UF_unchecked[rv]; //-||-
				
				int muv=mu;
				if (mv !=-1){
					if (mu==-1) muv=mv; 
					if (type[mv]==CORE){ //prefer the one from CORE if possible
						muv=mv;
					}
				}
				if (muv!=-1){	
						if (muv==mv) gg_UF_unchecked[rv]=-1; //signal it's used i.e marked
						if (muv==mu) gg_UF_unchecked[ru]=-1; //signal it's used i.e marked
						bg_marked[x]=MARKED;
						gg_checked[muv]=1;
						if (mu!=mv)
						int rx=uf_union(ru,rv,gg_UF,gg_UF_size,gg_UF_unchecked); //STEP-2 DONE //STEP-3 DONE 
						int yu=boys[2*muv];
						int yv=boys[2*muv+1];
						del_edge(muv,yu,yv,type, bg_xids,bg_ids,bg_degree,bg_marked,cdegree,bg_treestack,trees,bg_treevert); //STEP-4
	 				break;
	 			}	
	 		}
	} 
 	//return ;
 	free(gg_UF_unchecked); //1
 	free(gg_UF_size); //2
 	free(gg_UF); //3
 	///return;

 	free(bg_degree); //5
 	free(bg_ids); //7 
 	free(bg_xids); //8
 	free(bg_treestack); //9
 	free(cdegree); //6
 	free(type); //10
 	free(q1_bfs); free(q2_bfs); free(v1_bfs); free(v2_bfs);
 	free(bg_treevert);
 		for (int i=0;i<trees.size();++i){
 		trees[i].clear();
 		trees[i].shrink_to_fit();

 	}
 	trees.clear();
 	trees.shrink_to_fit();
}