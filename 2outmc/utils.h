#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <vector>
using namespace std;
struct sedge{
	int to;
	int which;
} 
typedef edge;
struct streevertice{
	int treeid;
	int pos;
	int order;
}typedef treev;
void select_oneside(int n,int *xids,int *ids,double *pr, int *choices);
void select_girls_smartly(int n,int *girls,int *xids,int *ids,double *pr,int *type);
void select_girl_smartly(int x, int n,int *girls,int *xids,int *ids,double *pr,int *type);
int uf_find(int pprime, int *gg_UF); 
int uf_union(int ru,int rv,int *gg_UF,int *gg_UF_size,int *gg_UF_unchecked);
void find_core(int n, int *bg_xids,edge* bg_ids,int *bg_marked,int *type,int *cdegree,int *boys,int *bg_treestack,vector<vector<int>> &trees,treev* bg_treevert,int *num_trees,int *q1_bfs,int *v1_bfs);
void krv_init(int n,int *boys,int *bg_marked,int *bg_degree,int *bg_xids,int *type,int *cdegree,edge *bg_ids,int *gg_UF,int *gg_UF_size,int *gg_UF_unchecked,int *gg_checked,treev *bg_treevert);
void cycleRelabel(int u,int *bg_xids,edge* bg_ids, int *type,int *cdegree,int *q1_bfs,vector<vector<int>> &trees,int *num_trees,treev *bg_treevert,int *bg_treestack);
