#include <vector>
//#include "ks.c"
#define CORE 0
#define TREE 1
#define FRINGE 2
#define DEAD 3
#define MARKED  1
#define UNMARKED  0
#define CHECKED 1
#define UNCHECKED 0
#include "utils.h"
using namespace std;

void select_oneside(int n,int *xids,int *ids,double *pr, int *choices){
		for (int i=0;i<n;++i){
			double sum=0;
			int sptr=xids[i];
			int eptr=xids[i+1];
			for (int j=sptr;j<eptr;++j) sum+=pr[ids[j]];
			double p =((double) rand() / (RAND_MAX))* sum;
			int ptr=sptr;
			double qsum=0;
			int sel1=-1;
			int sel2=-1;
			while ( ptr < eptr ){
				int u=ids[ptr];
				if ( p <= qsum + pr[u]){
					sel1=u;
					break;
				}
				qsum+=pr[u];
				ptr++;
			}
			p=((double) rand() / (RAND_MAX))*  (sum - pr[sel1]);
			ptr=sptr;
			qsum=0;
			while (ptr<eptr){
				int u=ids[ptr];
				if (u!=sel1){
					if ( p <= qsum + pr[u]){
						sel2=u;
						break;
					}
					qsum+=pr[u];
				}
				ptr++;
			}
			choices[2*i+1]=sel1;
			choices[2*i]=sel2;
	}
	
}

void select_girl_smart(int x, int n,int *girls, int *xids,int *ids,double *pr,int *type){
	int sptr=xids[x];
	int eptr=xids[x+1];
	double tfsum=0;
	double csum=0;
	int inC=0;
	double tmp=0;
	int u;
	int c1=-1,c2=-1;
	for (int ptr=sptr; ptr <eptr ;++ptr){
		u=ids[ptr];
		if (type[u]==CORE){
				inC++;
				csum+=pr[u];
				if (c1==-1) 
					c1=u;
				else if (c2==-1)
					c2=u;
			}else{
				tfsum+=pr[u];
			}
	}
	int sel1,sel2,ptr;
	if (inC==1){  //select one randomly!
		sel1=c1; 
		double p =((double) rand() / (RAND_MAX))*  tfsum;
		ptr=sptr;
		double qsum=0;
		while ( ptr < eptr ){
			u=ids[ptr];
			if (u!=c1){
				if ( p <= qsum + pr[u]){
					sel2=u;
					break;
				}
				qsum+=pr[u];
			}
			ptr++;
		}
	}else if (inC==2){ // simplest case, only two good ones!
		sel1=c1;
		sel2=c2;
	}else if (inC>2){ //  select  two good ones  randomly
		double p =((double) rand() / (RAND_MAX))* csum;
		ptr=sptr;
		double qsum=0;
		while ( ptr < eptr ){
			u=ids[ptr];
			if (type[u]==CORE){
				if ( p <= qsum + pr[u]){
					sel1=u;
					break;
				}
				qsum+=pr[u];
			}
			ptr++;
		}
		p=((double) rand() / (RAND_MAX))*  (csum - pr[sel1]);
		ptr=sptr;
		qsum=0;
		while ( ptr < eptr ){
			u=ids[ptr];
			if (u!=sel1 && type[u]==CORE){
				if ( p <= qsum + pr[u]){
					sel2=u;
					break;
				}
				qsum+=pr[u];
			}
			ptr++;
		}
	}else { //useless case (will be dropped)
		double p =((double) rand() / (RAND_MAX))* tfsum;
		ptr=sptr;
		double qsum=0;
		while ( ptr < eptr ){
			u=ids[ptr];
			if ( p <= qsum + pr[u]){
				sel1=u;
				break;
			}
			qsum+=pr[u];
			ptr++;
		}
		p=((double) rand() / (RAND_MAX))*  (tfsum - pr[sel1]);
		ptr=sptr;
		qsum=0;
		while ( ptr < eptr ){
			u=ids[ptr];
			if (u!=sel1){
				if ( p <= qsum + pr[u]){
					sel2=u;
					break;
				}
				qsum+=pr[u];
			}
			ptr++;
		}
	}
	girls[2*x+1]=sel1;
	girls[2*x]=sel2;
}
void select_girls_smartly(int n,int *girls,int *xids,int *ids,double *pr,int *type){
	for (int i=0;i<n;++i){
		select_girl_smart(i,n,girls,xids,ids,pr,type);
	}
}
int uf_find(int pprime, int *gg_UF)
{
    int q, s, sparent;
    for (q = pprime ; q != gg_UF [q] ; q = gg_UF [q])
        ;

    for (s = pprime ; s != q ; s = sparent) {
        sparent = gg_UF [s] ;    // path compression 
        gg_UF [s] = q ;
    }
    return (q) ;     
} 
 int uf_union(int ru,int rv,int *gg_UF,int *gg_UF_size,int *gg_UF_unchecked){
		int su=gg_UF_size[ru];
		int sv=gg_UF_size[rv];
		if (su>sv){
			gg_UF[rv]=ru;
			gg_UF_size[ru]+=sv;
			if (gg_UF_unchecked[ru]==-1){
				gg_UF_unchecked[ru]=gg_UF_unchecked[rv];			
			}

			return ru;
		}
		else{
			gg_UF[ru]=rv;
			gg_UF_size[ru]+=su;
			if (gg_UF_unchecked[rv]==-1){
				gg_UF_unchecked[rv]=gg_UF_unchecked[ru];			
			}

			return rv;
		}

}
void find_core(int n, int *bg_xids,edge* bg_ids,int *bg_marked,int *type,int *cdegree,int *boys,int *bg_treestack,vector<vector<int>> &trees,treev* bg_treevert,int *num_trees,int *q1_bfs,int *v1_bfs){
	int *td=(int*)malloc(n*sizeof(int));
	int *d1Stack=(int*)malloc(n*sizeof(int));
	for (int i=0;i<n;++i) {
		td[i]=bg_xids[i+1]-bg_xids[i];
		cdegree[i]=td[i];
	}
	int nt=0;

	for (int i=0;i<n;++i){
		if (v1_bfs[i]==-1){
			int  num_vert=0;
			int num_edges=0;
			int pos=0;
			int cpos=0;
			q1_bfs[pos++]=i;
			v1_bfs[i]=0;
			int d1cpos=0;
			int d1pos=0;
			while (cpos<pos){
				int x=q1_bfs[cpos++];
				num_vert++; num_edges+=td[x];
				if (td[x]==1) d1Stack[d1pos++]=x;
				for (int ptr=bg_xids[x];ptr<bg_xids[x+1];++ptr){
					int y= bg_ids[ptr].to;
					if (v1_bfs[y]==-1){
						v1_bfs[y]=0;
						q1_bfs[pos++]=y;
					}
				}
			}
			num_edges/=2;
			if (num_edges==num_vert-1){
				cpos=0;
				int newtreeid=trees.size();
				trees.push_back(vector<int>()); //can be a "resize"
				bg_treestack[nt++]=newtreeid;
				while (cpos<pos){
					int x=q1_bfs[cpos];
					trees[newtreeid].push_back(x);
					bg_treevert[x].pos=cpos; cpos++;
					bg_treevert[x].treeid=newtreeid; bg_treevert[x].order=2;
					num_vert++; num_edges+=td[x];
					for (int ptr=bg_xids[x];ptr<bg_xids[x+1];++ptr){
						int eid=bg_ids[ptr].which;
						int y=bg_ids[ptr].to;
						if (type[eid]!=TREE){
							type[eid]=TREE;
							cdegree[x]--;
							cdegree[y]--;
						}
					
					}
				}
			}else{
				while (d1cpos<d1pos){
					int x= d1Stack[d1cpos++];
					for (int ptr=bg_xids[x];ptr<bg_xids[x+1];++ptr){
						int y=bg_ids[ptr].to;
						int eid=bg_ids[ptr].which;
						if (type[eid]==CORE){
							type[eid]=FRINGE;
							td[y]--;
							cdegree[x]--; cdegree[y]--;
							if (td[y]==1) {d1Stack[d1pos++]=y;}
							break;
						}
					}
				}
			}
		}
	}
	free(td);
	free(d1Stack);	
	*num_trees=nt;
}
void krv_init(int n,int *boys,int *bg_marked,int *bg_degree,int *bg_xids,int *type,int *cdegree,edge *bg_ids,int *gg_UF,int *gg_UF_size,int *gg_UF_unchecked,int *gg_checked,treev *bg_treevert){

	for (int i=0;i<n;++i) bg_degree[i]=0; 
	for (int i=0;i<n;++i){
		bg_degree[boys[2*i]]++;
		bg_degree[boys[2*i+1]]++;
		bg_marked[i]=0;
		gg_checked[i]=0;
		gg_UF[i]=i;
		gg_UF_size[i]=1;
		gg_UF_unchecked[i]=i;
		type[i]=CORE;
		cdegree[i]=0;
	}
	int curr=0;
	bg_xids[0]=0;
	for (int i=1;i<=n;++i){
		bg_xids[i]=bg_xids[i-1] + bg_degree[i-1];		
		bg_degree[i-1]=0;
	}
	for (int i=0;i<n;++i){
		int u=boys[2*i];
		int v=boys[2*i+1];
		edge e1,e2;
		e1.to=v;
		e1.which=i;
		e2.to=u;
		e2.which=i;
		bg_treevert[i].pos=-1;
		bg_treevert[i].treeid=-1;
		bg_ids[bg_xids[u]+ bg_degree[u]++]=e1;
		bg_ids[bg_xids[v] + bg_degree[v]++]=e2;
	}
}
void cycleRelabel(int u,int *bg_xids,edge* bg_ids, int *type,int *cdegree,int *q1_bfs,vector<vector<int>> &trees,int *num_trees,treev *bg_treevert,int *bg_treestack){
		int pos=0;
		int cpos=0;
		int nt=*num_trees;
		if (cdegree[u]==1){
			int unicyclic=1;
			int curr=u;
			while (cdegree[curr]==1){
				cdegree[curr]=0;
				for (int ptr=bg_xids[curr];ptr<bg_xids[curr+1];++ptr){
					int eid=bg_ids[ptr].which;
					if (type[eid]==CORE){
						int y=bg_ids[ptr].to;
						cdegree[y]--;
						type[eid]=FRINGE;
						curr=y;
						break;
					}
				}
			}
			if (cdegree[curr]>0) unicyclic=0;			
			if (unicyclic){ //it was a unicylic component (based on our code addition), hence now it becomes a tree
				pos=0;
				cpos=0;
				q1_bfs[pos++]=u;
				int newtreeid=trees.size();
				trees.push_back(vector<int>());
				bg_treestack[nt++]=newtreeid;
				int q=0;
				while (cpos<pos){
					int x=q1_bfs[cpos++];
					trees[newtreeid].push_back(x);
					bg_treevert[x].pos=q++;
					bg_treevert[x].treeid=newtreeid;
					bg_treevert[x].order=2;
					for (int ptr=bg_xids[x];ptr<bg_xids[x+1];++ptr){
					int eid=bg_ids[ptr].which;
					if (type[eid]==FRINGE){
						int y=bg_ids[ptr].to;
						type[eid]=TREE;
						q1_bfs[pos++]=y;
					}
				} 
			
			}

		}
	}
	*num_trees=nt;
}
