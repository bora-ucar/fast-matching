#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <vector>
#include "utils.h"
using namespace std;

void karpSipser(int n, int *Acptrs, int *Acids, int *Arptrs, int *Arids, int *match,int *match2);
void del_edge(int index,int u,int v,int *type, int *bg_xids,edge *bg_ids,int *bg_degree,int *bg_marked,int *cdegree,int *bg_treestack,vector<vector<int>> &trees,treev *bg_treevert);
void twoutmc(int n, int *boys,int *girls,int *bg_marked,int *gg_checked,int *rxids,int *rids,int *cxids,int *cids,double *sr, double *sc);
