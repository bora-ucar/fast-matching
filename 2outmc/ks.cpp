#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>



/********************************************************************/
void updateDegs(int u, int *Aptrs, int *Aids, int *match, int *degs,
                int *d1lst, int *_d1st, int *_d1end)
{
    int j, v;
    int d1st = *_d1st, d1end = *_d1end;

    for (j = Aptrs[u]; j < Aptrs[u+1]; j++)
    {
        v = Aids[j];

        
        if (match[v] == -1)
        {
            degs[v] --;
            if (degs[v] == 1){

                d1lst[++d1end] = v;
            }
        }
    }
    *_d1st = d1st;
    *_d1end = d1end;
}
/********************************************************************/

/********************************************************************/
void karpSipser(int n, int *Acptrs, int *Acids, int *Arptrs, int *Arids, int *match,int *match2)
{
    /*ids from 0 to n-1*/
    
    int *c_d1lst, *r_d1lst;
    int *c_degs,*r_degs;
    int i, c_d1st, c_d1end, r_d1st,r_d1end, u, v, j;
    
    
    int *edgeI, randEdgeSt, nnz = Arptrs[n];
    int *edgeJ;
    int flag;
    
    
    c_d1lst = (int *) calloc(n+3, sizeof(int));/*3n is more than what we need but let us allocate*/
    r_d1lst = (int *) calloc(n+3, sizeof(int));/*3n is more than what we need but let us allocate*/
    c_degs = (int *) calloc(n, sizeof(int));
    r_degs = (int *) calloc(n, sizeof(int));
    edgeI = (int *) calloc(nnz, sizeof(int));
    edgeJ = (int *) calloc(nnz, sizeof(int));
    
    c_d1st = 0; c_d1end = -1;
    r_d1st = 0; r_d1end = -1;

    for (i = 0; i < n; i++){
        match[i] = -1;
        match2[i]= -1;
    }
    for (i = 0; i < n; i++)
    {
        c_degs[i] =Acptrs[i+1] - Acptrs[i];
        r_degs[i] =Arptrs[i+1] - Arptrs[i];
        if (c_degs[i] == 1)
            c_d1lst[ ++c_d1end] = i;
        if (r_degs[i] == 1)
            r_d1lst[ ++r_d1end] = i;
        for (j = Acptrs[i]; j < Acptrs[i+1]; j++)
        {
            edgeI[j] = i;
            edgeJ[j] = Acids[j];
        }
    }
    

    randEdgeSt = 0;
    
    
    flag = 1;
    while (flag)
    {
        while (c_d1end >= c_d1st)
        {
            u = c_d1lst[c_d1st++];
            if (match[u]!=-1)
                continue;
            for (j = Acptrs[u]; j < Acptrs[u+1]; j++)  {
                v = Acids[j];
                if (match2[v] == -1) {
                    match2[v] = u;
                    match[u] = v;

                    break;
                }
            }
            if (match[u] != -1) {
                updateDegs(u, Acptrs, Acids, match2, r_degs, r_d1lst, &r_d1st, &r_d1end);
                updateDegs(match[u], Arptrs, Arids, match, c_degs, c_d1lst, &c_d1st, &c_d1end);
                
            }
            
        }
        while (r_d1end >= r_d1st)
        {
            v = r_d1lst[r_d1st++];
            if (match2[v]!=-1)
                continue;
            for (j = Arptrs[v]; j < Arptrs[v+1]; j++)  {
                u = Arids[j];
                if (match[u] == -1) {
                    match2[v] = u;
                    match[u] = v;

                    break;
                }
            }
            if (match2[v] != -1) {
                updateDegs(u, Acptrs, Acids, match2, r_degs, r_d1lst, &r_d1st, &r_d1end);
                updateDegs(match[u], Arptrs, Arids, match, c_degs, c_d1lst, &c_d1st, &c_d1end);

                
            }
            
        }
        
        
        while (randEdgeSt < nnz ) {
            
            u = edgeI[randEdgeSt];
            v = edgeJ[randEdgeSt++];
            if (match[u] == -1 && match2[v] == -1) {
                match[u] = v;
                match2[v] = u;

                updateDegs(u, Acptrs, Acids, match2, r_degs, r_d1lst, &r_d1st, &r_d1end);
                updateDegs(match[u], Arptrs, Arids, match, c_degs, c_d1lst, &c_d1st, &c_d1end);

                break;
            }
        }
        
        if(randEdgeSt>=nnz &&  (c_d1end < c_d1st &&  r_d1end < r_d1st) )
            flag = 0;
    }/*while flag*/
    
    free(edgeJ);
    free(edgeI);
    free(r_degs);
    free(r_d1lst);
    free(c_degs);
    free(c_d1lst);
}




